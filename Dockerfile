FROM alpine:3.20
LABEL Description="Uma pequena, nem tanto assim (sic), imagem para você testar o poder do Enyalius"

RUN apk --update add apache2 apache2-ssl \
          curl rsync openssh lftp poppler-utils \
          php83 php83-apache2 php83-curl php83-json php83-openssl php83-simplexml php83-xml php83-dom \
          php83-gd php83-pecl-xdebug php83-session php83-pdo_pgsql php83-pdo_mysql \
          php83-mbstring php83-iconv php83-tokenizer php83-xmlwriter php83-ctype \
          php83-gmp php83-phar
          
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer 

RUN rm -f /var/cache/apk/* \
    && mkdir -p /opt/utils 

EXPOSE 80 443 9000

ADD start.sh /opt/utils/

RUN chmod +x /opt/utils/start.sh
COPY ssl.conf /etc/apache2/conf.d/ssl.conf

ENTRYPOINT ["/opt/utils/start.sh"]
