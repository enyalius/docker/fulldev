# Server
Imagem Docker para iniciar projetos com PHP e Apache, especialmente e carinhosamente preparado para rodar o Enyalius.

## Como usar

### Iniciando o container
Usando o Eny você pode iniciar o container com o comando (porta 9090):

```
eny server start
```
Se quiser também tem o eny eny 😜
```
eny eny 9090
```

Ou usando o Docker diretamente:

